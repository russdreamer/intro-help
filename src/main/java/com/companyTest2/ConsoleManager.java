package com.companyTest2;

import com.companyTest2.dao.Repository;
import com.companyTest2.dao.UsersCachedRepositoryImpl;
import com.companyTest2.entity.User;
import com.companyTest2.exceptions.FileReadException;
import com.companyTest2.exceptions.FileWriteException;
import com.companyTest2.filesystemworker.UserFSWorkerImpl;

import java.io.File;
import java.util.InputMismatchException;
import java.util.Optional;
import java.util.Scanner;

public class ConsoleManager {
    private final Scanner scanner;
    private Repository repository;

    public ConsoleManager() {
        scanner = new Scanner(System.in);
    }

    public void start() throws FileReadException {
        System.out.println("Введите имя файла с данымми:");
        String fileName = scanner.nextLine(); //users.txt
        File file = new File(fileName);
        if (!file.exists()) {
            System.err.println("Неверный путь файла.");
            return;
        }


        repository = new UsersCachedRepositoryImpl(new UserFSWorkerImpl(file));

        String introMessage = """
                Выберите одну из команд:
                Найти - для поиска пользователя по ID
                Добавить - для добавления нового пользователя
                Обновить - для обновления данных пользователя
                Выход - для завершения работы с приложением""";
        String command = "";
        System.out.println(introMessage);
        while (!"выход".equalsIgnoreCase(command = scanner.nextLine())) {
            switch (command.toLowerCase()) {
                case "найти" -> findById().ifPresentOrElse(
                        System.out::println, () -> System.err.println("Объекта с таким id не существует."));
                case "добавить" -> addUser();
                case "обновить" -> updateUser();
                default -> System.err.println("Данной команды не существует!");
            }
            System.out.println(introMessage);
        }
    }

    private void updateUser() {
        Optional<User> user = findById();
        if (user.isEmpty()) {
            System.err.println("Объекта с таким id не существует.");
            return;
        }

        System.out.println("Выберите номер поля, которое хотите изменить, где\n1 - name\n2 - age\n3 - isWorker");
        int number = getPositiveIntByForce();

        //TODO(krasilnikov): дописать самостоятельно
    }

    private void addUser() {
        User.UserBuilder builder = new User.UserBuilder();
        String name;
        System.out.println("Введите имя:");
        while ((name = scanner.nextLine()).isEmpty()) {
            System.err.println("Имя должно содержать хотя бы 1 символ! Попробуйте ещё раз:");
        }
        builder.setName(name);
        System.out.println("Введите возраст:");
        builder.setAge(getPositiveIntByForce());
        Integer isWorkerNum = null;
        while (isWorkerNum == null) {
            //TODO(krasilnikov): ловушка
            System.out.println("Является ли он работником?\n1 - да\n2 - нет\n3 - пидора ответ \uD83D\uDE06");
            isWorkerNum = getPositiveIntByForce();
            switch (isWorkerNum) {
                case 1 -> builder.setWorker(true);
                case 2 -> builder.setWorker(false);
                default -> {
                    System.out.println("Неверное значение! Ответ может быть только 1 или 2.");
                    isWorkerNum = null;
                }
            }
        }
        try {
            long newId = repository.addUser(builder.build());
            System.out.println("Пользователь успешно добавлен под ID: " + newId);
        } catch (FileWriteException e) {
            System.err.println(e.getMessage());
        }
    }

    private int getPositiveIntByForce() {
        Integer number = null;
        while (number == null) {
            try {
                number = Integer.parseInt(scanner.nextLine());
                if (number < 0) {
                    System.err.println("Ошибка! Значение может быть только положительным числом.");
                    number = null;
                }
            } catch (NumberFormatException ignore) {
                System.err.println("Введено неверное значение. Ответ может быть только числом. Попробуйте ещё раз:");
                number = null;
            }
        }
        return number;
    }

    private Optional<User> findById() {
        System.out.println("Введите id пользователя:");
        try {
            return repository.findById(scanner.nextLong());
        } catch (InputMismatchException e) {
            System.err.println("Введено неверное значение! ID должно содержать только цифры.");
        }
        return Optional.empty();
    }
}
