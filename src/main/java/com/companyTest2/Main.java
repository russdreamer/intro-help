package com.companyTest2;

import com.companyTest2.exceptions.FileReadException;

public class Main {
    public static void main(String[] args) {
        ConsoleManager manager = new ConsoleManager();
        try {
            manager.start();
        } catch (FileReadException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.err.println("Произошла непредвиденная ошибка: " + e.getMessage());
        }
    }
}
