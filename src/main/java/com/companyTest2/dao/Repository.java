package com.companyTest2.dao;

import com.companyTest2.entity.User;
import com.companyTest2.exceptions.FileWriteException;

import java.util.Optional;

public interface Repository {
    /**
     * Adds user if it doesn't exist
     * @param user user to add
     * @return user ID if user added successfully
     */
    long addUser(User user) throws FileWriteException;
    /**
     * Finds user by its Id
     * @param id of user in database
     * @return user with provided ID if it exists
     */
    Optional<User> findById(long id);
    /**
     * Updates user entity
     * @param user to update
     * @return <code>True</code> if user was updated successfully or <code>False</code> if the user doesn't exist
     */
    boolean update(User user) throws FileWriteException;
}
