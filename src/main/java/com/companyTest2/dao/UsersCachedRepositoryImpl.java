package com.companyTest2.dao;

import com.companyTest2.entity.User;
import com.companyTest2.exceptions.FileReadException;
import com.companyTest2.exceptions.FileWriteException;
import com.companyTest2.filesystemworker.FSWorker;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

public class UsersCachedRepositoryImpl implements Repository{
    private Map<Long, User> usersCache;
    private FSWorker worker;

    public UsersCachedRepositoryImpl(FSWorker fSWorker) throws FileReadException {
        this.worker = fSWorker;
        this.usersCache = worker.parseFile();
    }

    @Override
    public long addUser(User user) throws FileWriteException {
        long id = getFreeId();
        if (id == -1) {
            System.err.println("База данных полностью заполненна.");
        }
        user.setId(id);

        usersCache.put(user.getId(), user);
        worker.addAll(usersCache);
        return id;
    }

    private long getFreeId() {
        Set<Long> keys = usersCache.keySet();
        for (long i = 1; i < Long.MAX_VALUE; i++) {
            if (!keys.contains(i)) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public Optional<User> findById(long id) {
        return Optional.ofNullable(usersCache.get(id));
    }

    @Override
    public boolean update(User user) throws FileWriteException {
        if (!usersCache.containsKey(user.getId())) {
            return false;
        }
        if (!worker.update(user)) {
            return false;
        }
        usersCache.put(user.getId(), user);
        return true;
    }
}
