package com.companyTest2.entity;

public class User {
    private long id;
    private String name;
    private int age;
    private boolean isWorker;

    public User(int id, String name, int age, boolean isWorker) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.isWorker = isWorker;

    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean getIsWorker() {
        return isWorker;
    }

    public void setWorker(boolean worker) {
        isWorker = worker;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", isWorker=" + isWorker +
                '}';
    }

    public static class UserBuilder {
        private String name;
        private int age;
        private boolean isWorker;

        public UserBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder setAge(int age) {
            this.age = age;
            return this;
        }

        public UserBuilder setWorker(boolean worker) {
            isWorker = worker;
            return this;
        }

        public User build() {
            return new User(0, name, age, isWorker);
        }
    }
}
