package com.companyTest2.filesystemworker;

import com.companyTest2.entity.User;
import com.companyTest2.exceptions.FileReadException;
import com.companyTest2.exceptions.FileWriteException;

import java.util.Map;

public interface FSWorker {
    boolean addUser(User user) throws FileWriteException;
    boolean addAll(Map<Long, User> users) throws FileWriteException;
    Map<Long, User> parseFile() throws FileReadException;
    boolean update(User user) throws FileWriteException;
}
