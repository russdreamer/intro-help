package com.companyTest2.filesystemworker;

import com.companyTest2.entity.User;
import com.companyTest2.exceptions.FileReadException;
import com.companyTest2.exceptions.FileWriteException;

import java.io.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class UserFSWorkerImpl implements FSWorker {
    private final File file;

    public UserFSWorkerImpl(File file) {
        this.file = file;
    }

    @Override
    public boolean addUser(User user) throws FileWriteException {
        //TODO(krasilnikov): планируется реализовать в воркере с точечным доступом в файл
        throw new RuntimeException("Данный метод не поддерживается текущим воркером");
    }

    @Override
    public boolean addAll(Map<Long, User> userMap) throws FileWriteException {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(file, false))) {
            for (User user1 : userMap.values()) {
                bufferedWriter.write(user1.getId() + "|"
                        + user1.getName() + "|" + user1.getAge() + "|" + user1.getIsWorker());
                bufferedWriter.newLine();
            }
        } catch (IOException e) {
            throw new FileWriteException("Ошибка перезаписи файла.");
        }
        return true;
    }

    @Override
    public Map<Long, User> parseFile() throws FileReadException {
        Map<Long, User> userMap = new LinkedHashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            reader.lines()
                    .forEach(line -> {
                        String[] lineParametersUser = line.split(Pattern.quote("|"));
                        userMap.put(Long.parseLong(lineParametersUser[0]),
                                new User(Integer.parseInt(lineParametersUser[0]), lineParametersUser[1],
                                        Integer.parseInt(lineParametersUser[2]),
                                        Boolean.parseBoolean(lineParametersUser[3])));
                    });
        } catch (IOException e) {
            throw new FileReadException("Ошибка чтения файла.");
        }
        return userMap;
    }

    @Override
    public boolean update(User user) throws FileWriteException {
        //TODO(krasilnikov): реализовать
        return false;
    }
}
