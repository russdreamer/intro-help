package com.companyTest2.exceptions;

public class FileWriteException extends Exception {
    public FileWriteException(String message) {
        super(message);
    }
}
